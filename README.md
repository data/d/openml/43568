# OpenML dataset: Metal-Concentrations

https://www.openml.org/d/43568

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
Many kids' products are thought to contain dangerous metals. Due to this it is important to make sure that children can enjoy their toys while staying as safe as possible.
Content
In this dataset you'll find numerous products tested for dangerous metals by the New York City Health Department.
Acknowledgements
This data comes from https://data.cityofnewyork.us/Health/Metal-Content-of-Consumer-Products-Tested-by-the-N/da9u-wz3r.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43568) of an [OpenML dataset](https://www.openml.org/d/43568). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43568/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43568/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43568/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

